CREATE TABLE IF NOT EXISTS Stock(
    product_name VARCHAR(255) PRIMARY KEY,
    in_stock INT NOT NULL,
    warning_threshold INT(4) NOT NULL,
    supply_threshold INT(4) NOT NULL,
    auto_request_enabled BOOLEAN,
    auto_request_quantity INT(4) NOT NULL
);

CREATE TABLE IF NOT EXISTS OfferHistory(
    offer_id VARCHAR(32) NOT NULL,
    supplier VARCHAR(32) NOT NULL,
    request_timestamp DATETIME NOT NULL,
    offer_timestamp DATETIME NOT NULL,
    estimated_arrival_timestamp DATETIME NOT NULL,
    delay INT DEFAULT 0 NOT NULL,
    ok BOOLEAN DEFAULT 1,
    PRIMARY KEY (offer_id, supplier)
);