import pandas
import schedule
import time
import functools
import requests
import json
from pprint import pprint

from database import consume_from_stock
from database import get_products_need_to_request_for_new_stocks
from database import update_products_for_request_has_been_sent

app_port = 5000

def consume(product, quantity):
    consume_from_stock(product, quantity)

def products_flow():
    print("reading from stock_consumers")
    consumers_df = pandas.read_csv('stock_consumers.csv')

    schedule.every(10).seconds.do(functools.partial(verify_products))

    for index, row in consumers_df.iterrows():
        schedule.every(row['rate_in_seconds']).seconds.do(functools.partial(consume,row['product'], row['quantity']))

    while True:
        schedule.run_pending()
        time.sleep(1)

def verify_products():
    print("verify if automatic requests need to be done")
    results = get_products_need_to_request_for_new_stocks()
    if len(results) > 0:
        products = []
        product_names = []
        for result in results:
            products.append({"name" : result[0], "quantity" : result[1]})
            product_names.append(result[0])

        body = {
            "products": products
        }
        r = requests.post(url=f"http://localhost:{app_port}/supplier/add-request", data=json.dumps(body))
        if r.status_code == 200:
            pprint(r.content)
            update_products_for_request_has_been_sent(product_names)


def start():
    products_flow()
