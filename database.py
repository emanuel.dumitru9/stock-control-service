import datetime

import mysql.connector
import simplejson as json
import pandas

def getConnection():
    connection = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="stock_control_service",
        auth_plugin='mysql_native_password'
    )
    return connection


class StockEntry:
    def __init__(self, product_name, in_stock, warning_threshold, supply_threshold, auto_request_enabled,
                 auto_request_quantity):
        self.product_name = product_name
        self.in_stock = in_stock
        self.warning_threshold = warning_threshold
        self.supply_threshold = supply_threshold
        self.auto_request_enabled = auto_request_enabled
        self.auto_request_quantity = auto_request_quantity

    def as_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)


def create_env():
    file = open("database_env.sql", "r")
    sql = file.read()

    connection = getConnection()
    sqls = sql.split('\n\n')
    for query in sqls:
        mycursor = connection.cursor()
        mycursor.execute(query)
        connection.commit()
        mycursor.close()
    connection.close()


def getStock():
    connection = getConnection()
    mycursor = connection.cursor()
    mycursor.execute("SELECT * FROM Stock;")
    myresult = mycursor.fetchall()
    mycursor.close()
    connection.close()

    return json.dumps(
        list(map(lambda entry: StockEntry(entry[0], entry[1], entry[2], entry[3], entry[4], entry[5]), myresult)),
        default=lambda o: o.__dict__, indent=4)


def consume_from_stock(product_name, quantity):
    connection = getConnection()
    mycursor = connection.cursor()

    sql = f"UPDATE Stock SET in_stock = in_stock - {quantity} WHERE product_name = '{product_name}'"

    mycursor.execute(sql)
    connection.commit()
    mycursor.close()
    connection.close()


def get_products_need_to_request_for_new_stocks():
    connection = getConnection()
    mycursor = connection.cursor()
    mycursor.execute(
        "SELECT product_name, auto_request_quantity FROM Stock WHERE in_stock <= supply_threshold AND auto_request_enabled = 1")
    myresult = mycursor.fetchall()
    mycursor.close()
    connection.close()

    return myresult


def update_products_for_request_has_been_sent(products):
    connection = getConnection()
    mycursor = connection.cursor()
    list_of_products = "(%s)" % format(str(products).replace("[", "").replace("]", ""))

    sql = f"UPDATE Stock SET auto_request_enabled = 0 WHERE product_name IN {list_of_products}"

    mycursor.execute(sql)
    connection.commit()
    mycursor.close()
    connection.close()


def uploadStock(file):
    stock_df = pandas.read_csv(file)
    connection = getConnection()
    mycursor = connection.cursor()

    sql = "INSERT INTO Stock (product_name,in_stock,warning_threshold,supply_threshold,auto_request_enabled,auto_request_quantity) VALUES \n"

    for index, row in stock_df.iterrows():
        if index != 0:
            sql += ","
        sql += f'("{row["product_name"]}",{row["in_stock"]},{row["warning_threshold"]},{row["supply_threshold"]},{row["auto_request_enabled"]},{row["auto_request_quantity"]})'
    sql += " ON DUPLICATE KEY UPDATE in_stock = in_stock + VALUES(in_stock)"

    mycursor.execute(sql)
    connection.commit()
    mycursor.close()
    connection.close()


class OffersEntry:
    def __init__(self, supplier, ok, value):
        self.ok = ok
        self.supplier = supplier
        self.value = value


class AnalyticsEntry:
    def __init__(self, supplier, value):
        self.supplier = supplier
        self.value = value


def myconverter(o):
    if isinstance(o, datetime.date):
        return o.__str__()


def get_count_evolution():
    connection = getConnection()
    mycursor = connection.cursor()
    mycursor.execute("select supplier, DATE(estimated_arrival_timestamp + INTERVAL delay HOUR) as date, count(*) from OfferHistory where ok = 1 group by supplier, date;")
    myresult = mycursor.fetchall()
    mycursor.close()
    connection.close()

    response_dict = {}

    for row in myresult:
        if row[0] in response_dict.keys():
            response_dict.get(row[0]).append((row[1], row[2]))
        else:
            response_dict[row[0]] = [(row[1], row[2])]

    return json.dumps(response_dict, default = myconverter)


def get_analytics_entries(query):
    connection = getConnection()
    mycursor = connection.cursor()
    mycursor.execute(query)
    myresult = mycursor.fetchall()
    mycursor.close()
    connection.close()

    return json.dumps(
        list(map(lambda entry: AnalyticsEntry(entry[0], entry[1]), myresult)),
        default=lambda o: o.__dict__, indent=4)


def get_analytics_offers():
    connection = getConnection()
    mycursor = connection.cursor()
    mycursor.execute("SELECT supplier, ok, count(*) FROM OfferHistory GROUP BY supplier, ok;")
    myresult = mycursor.fetchall()
    mycursor.close()
    connection.close()

    return json.dumps(
        list(map(lambda entry: OffersEntry(entry[0], entry[1], entry[2]), myresult)),
        default=lambda o: o.__dict__, indent=4)


def get_analytics_response_time():
    return get_analytics_entries("select supplier, avg(TIMESTAMPDIFF(MINUTE,request_timestamp,offer_timestamp)) as "
                                 "response_time FROM OfferHistory group by supplier;")


def get_analytics_eta():
    return get_analytics_entries("select supplier, avg(TIMESTAMPDIFF(HOUR,offer_timestamp,"
                                 "estimated_arrival_timestamp)) as response_time FROM OfferHistory group by supplier;")


def get_analytics_delays():
    return get_analytics_entries("select supplier, avg(delay) from OfferHistory group by supplier;")


def get_analytics_delivery_time():
    return get_analytics_entries("select supplier, avg(TIMESTAMPDIFF(HOUR,request_timestamp,"
                                 "(estimated_arrival_timestamp + INTERVAL delay HOUR))) as response_time FROM "
                                 "OfferHistory group by supplier;")
