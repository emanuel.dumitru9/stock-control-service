import json
import logging
import threading
import random
import time
from datetime import datetime, timedelta
from pprint import pprint

from flask import Flask, request
from flask_cors import CORS

from database import getStock, uploadStock, create_env, get_analytics_offers, get_analytics_response_time, \
    get_analytics_eta, get_analytics_delays, get_analytics_delivery_time, get_count_evolution
from scheduled import start

"""
    Description here
"""
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)

app = Flask('Stock control service devhacks')
app_port = 5000

CORS(app)


"""
Config data and maybe hardcoded stuff goes here - for connection with analytics platform
"""
application_metadata = {
    "version" : "1.0.0",
    "name" : "stock-control-service"
}

"""
Abstract class for datasource
"""
class Datasource:
    name = "stock-control-service"
    connection = ""

    def __init__(self, hostname, user=None, password=None):
        logging.info("Initializing Datasource ...")
        self.hostname = hostname
        self.username = user
        self.password = password

    def description(self):
        return "(Hostname = {}, Username = {}, Password = {})".format(self.hostname, self.username, self.password)

"""
Inspiration
"""
class ElasticSearch(Datasource):
    port = 9200
    def work(self):
        """Write, Read from ElasticSearch
        """
        logging.info("Selected database ElasticSearch{} ...".format(super(ElasticSearch,self).description()))
        es_object = self.setup_connection()
        search_object = {'_source': ['title'], 'query': {'range': {'calories': {'gte': 200}}}}
        data = self.search(es_object, 'questions_ro', json.dumps(search_object))
        logging.info(data)

    def store_record(self, elastic_object, index_name, record):
        is_stored = True
        try:
            outcome = elastic_object.index(index=index_name, doc_type='question', body=record)
            logging.info(outcome)
        except Exception as ex:
            logging.error('Error in indexing data')
            logging.error(str(ex))
            is_stored = False
        finally:
            return is_stored

    def search(self, es_object, index_name, search):
        res = es_object.search(index=index_name, body=search)
        return res

    def setup_connection(self):
        pass
        # es = None
        # es = Elasticsearch([{'host': self.hostname, 'port': 9200}])
        # if es.ping():
        #     logging.info('Yay Connected')
        # else:
        #     logging.error('Awww it could not connect!')
        # return es


db = ElasticSearch('127.0.0.1')

es_object = db.setup_connection()


"""
    {
      'country' : '',
      'question' : ''
    }
"""

@app.route('/search-assistance-service', methods=['POST'])
def process_request():
    data = request.data
    dataDict = json.loads(data)

    question = dataDict['question']
    country = dataDict['country']

    language_detected = translator.detect(str(question))

    search_query = {
        "query": {
            "match": {
                "data": question
            }
        },
        "aggs": {
            "services": {
                "terms": {"field": "service"}
            }
        }
    }
    response = db.search(es_object, index_name='questions_' + language_detected.lang, search=search_query)
    map = {}

    logging.info(response)
    data_to_respond_with = []
    try:
       for hit in response['hits']['hits']:
           my_service = hit['_source']['service']
           my_score = hit['_score']
           print(my_score, my_service)

           if my_service not in map:
               map[my_service] = my_score
           else:
               map[my_service] = (map[my_service] + my_score) / 2

       pprint(map)



       service = sorted(map, key=map.get, reverse=True)[0]
       data_to_respond_with.append({
           "service_name" : service,
           "service_url"  : final_data_to_user.get((service, country))
       })
    except Exception as e:
        logging.error(e)

    response = app.response_class(
        response=json.dumps(data_to_respond_with),
        mimetype='application/json'
    )
    return response


@app.route('/get-assistance-services', methods=['GET'])
def get_services():
    response = app.response_class(
        response=json.dumps(data.data_services),
        mimetype='application/json'
    )
    return response

@app.route('/get-countries', methods = ['GET'])
def get_countries():
    response = app.response_class(
        response=json.dumps(data.data_countries),
        mimetype='application/json'
    )
    return response


@app.route('/get-languages', methods = ['GET'])
def get_languages():
    response = app.response_class(
        response=json.dumps(languages),
        mimetype='application/json'
    )
    return response



@app.route("/add-dataset-file", methods=['POST'])
def add_questions_file():
    service = request.headers['service']
    language = request.headers['language']
    file = request.files['file']
    file_data = file.readlines()

    data_to_be_added = []

    for line in file_data:
        question = line.decode("utf-8")
        question = str(question).strip("'<>() ").replace('\'', '\"').replace('\n', '')
        question = fix_text(question)

        data = {
            "question": question,
            "wordCount": len(question.split()),
            "service": service,
            "index": "questions_" + language
        }

        db.store_record(es_object, index_name=data["index"], record={
            "data": data["question"],
            "service": data["service"],
            "wordCount": data['wordCount']
        })

        for lang in languages:
            if lang != language:
                try:
                    question_translated = translator.translate(question, dest=lang)
                    data = {
                        "question": question_translated.text,
                        "wordCount": len(question_translated.text.split()),
                        "service": service,
                        "index": "questions_" + lang
                    }
                    db.store_record(es_object, index_name=data["index"], record={
                        "data": data["question"],
                        "service": data["service"],
                        "wordCount": data['wordCount']
                    })
                except Exception as e:
                    logging.error(e)

    response = app.response_class(
        status=200,
        mimetype='application/json'
    )
    return response



"""
    For each request : question / service / language
    Will push to elastic search for each language a record with question translated in that language 
    [
        'question' : '',
        'service'  : '', 
        'language' : ''
    ]

"""
@app.route('/add-dataset-record', methods=['POST'])
def add_question():
    data = request.data
    dataDict = json.loads(data)

    for item in dataDict:

        data_to_be_added = []

        question = item['question']
        service = item['service']
        langu = item['language']

        data_to_be_added.append({
            "question" : question,
            "wordCount" : len(question.split()),
            "service" : service,
            "index"   : "questions_" + langu
        })

        for lang in languages:
            if lang != langu:
                question_translated = translator.translate(question, dest=lang)
                data_to_be_added.append({
                    "question": question_translated.text,
                    "wordCount": len(question_translated.text.split()),
                    "service": service,
                    "index": "questions_" + lang
                })

        for data in data_to_be_added:
            db.store_record(es_object, index_name=data["index"], record={
                "data" : data["question"],
                "service"  : data["service"],
                "wordCount" : data['wordCount']
            })

    response = app.response_class(
        status=200,
        mimetype='application/json'
    )
    return response



"""
Our code for stock-control-service project, connection with analytics
"""

@app.route('/analytics/info', methods=['GET'])
def getApplicationAnalyticsInfo():
    response = app.response_class(
        response=json.dumps(application_metadata),
        mimetype='application/json'
    )
    return response





"""
Our code for stock-control-service project, connection with supplier
Config data and maybe hardcoded stuff goes here - for connections with the supplier


DB - suppliers_requests / suppliers_offers /  suppliers_transport_events / ?
read and writes from firebase ( will communicate with supplier through firebase )
"""

firebase_metadata = {
    "endpoint" : "localhost",
    "token" : "un-token",
    "version" : "1.0.0",
    "name" : "stock-control-service"
}


from firebase import Firebase

config = {
  "apiKey": "AIzaSyBHcgeRa7ExIJPBpPoYNGWqpB3VdhbIssw",
  "authDomain": "supplierscontrol-d22fe.firebaseapp.com",
  "databaseURL": "https://supplierscontrol-d22fe.firebaseio.com",
  "storageBucket": "supplierscontrol-d22fe.appspot.com"
}

firebase = Firebase(config)
db = firebase.database()

requests = {}
offers = {}
transports = {}


db_requests = "requests"
db_offers = "offers"
db_transports = "transports"

# create request on requests firestore table
request_transport = {
    "request_id" : "",
    "products": [
        {"name": "banana", "quantity": "600"}
    ],
    "timestamp" : "",
    "status": "", # WAITING, APPROVED, DECLINED,
    "supplier_id" : ""
}


# listen on all the offers received from offers firestore table and update status with declined or accepted
offer_transport  = {
    "offer_id": "",
    "request_id" : "",
    "price" : "",
    "estimate_arrival" : "",
    "timestamp": "",
    "status" : ""  # declined, accepted, in_progress
}

""" Platform analyze the offers per request and choose one. When an offer has been accepted for a request, create a transport event
After transport arrived update the stocks

"""

# Listen on transport events and send updates to the platform / update stocks when transport received
transport_event = {
    "transport_id": "",
    "offer_id": "",
    "request_id": "",
    "transport_details": "",
    "timestamp": "",
    "remaining_time" : "",
    "status": "" # in_progress, arrived
}


@app.route('/supplier/get-requests', methods=['GET'])
def getRequests():
    retrieved_requests = None
    error = ""
    try:
        retrieved_requests = db.child(db_requests).get()
    except Exception as ex:
        error = str(ex)

    if error == "":
        requests = mapper_firebase_application(retrieved_requests)

    my_response = {
        "response": {
            "results": requests,
            "error": error
        }
    }

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response


@app.route('/supplier/get-offers', methods=['POST'])
def getOffersForARequest():
    data = request.data
    dataDict = json.loads(data)
    request_id = dataDict['request_id']
    retrieved_offers = None
    error = ""
    offers = []
    try:
        retrieved_offers = db.child(db_offers).get()
    except Exception as ex:
        error = str(ex)

    if error == "":
        offers = mapper_firebase_application(retrieved_offers, key="request_id", value=request_id)

    my_response = {
        "response": {
            "results": offers,
            "error": error
        }
    }

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response


@app.route('/supplier/add-request', methods=['POST'])
def addRequestForTransport():
    data = request.data
    dataDict = json.loads(data)
    products = dataDict['products']

    request_transport = {
        "request_id": "",
        "products": products,
        "timestamp":  time.mktime(datetime.now().timetuple()),
        "status" : "WAITING",
        "company" : "METRO-SYSTEMS"
    }

    error = ""
    try :
        response = db.child(db_requests).push(request_transport)
        request_transport["request_id"] = response["name"]
        db.child(db_requests + "/" + response["name"]).update({"request_id" : response["name"]})
        pprint(response)
    except Exception as ex:
        error = str(ex)


    my_response = {
        "response" : {
            "request_id": request_transport["request_id"],
            "request_payload" : request_transport,
            "error": error
        }
    }

    if error == "":
        requests[request_transport["request_id"]] = my_response

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response

@app.route('/supplier/ack-offer', methods=['POST'])
def acceptAnOfferForTheRequestForTransport():
    data = request.data
    dataDict = json.loads(data)
    offer = dataDict['offer_id']
    requestAssociated = dataDict['request_id']

    error = ""
    try:
        db.child(db_offers).child(offer).update({"status" : "ACCEPTED"})
        db.child(db_requests).child(requestAssociated).update({"status": "CLOSED"})
    except Exception as ex:
        error = str(ex)

    my_response = {
        "response": {
             "result_id": offer,
             "result_message": "Accepted offer",
             "error": error
        }
    }

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response


@app.route('/supplier/decline-offer', methods=['POST'])
def declineAnOfferForTheRequestForTransport():
    data = request.data
    dataDict = json.loads(data)
    offer = dataDict['offer_id']
    requestAssociated = dataDict['request_id']

    error = ""
    try:
        db.child(db_offers).child(offer).update({"status": "DECLINED"})
    except Exception as ex:
        error = str(ex)

    my_response = {
        "response": {
            "result_id": offer,
            "result_message": "Declined offer",
            "error": error
        }
    }

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response


@app.route("/supplier/terminate-offer", methods=['POST'])
def terminateOfferAndReview():
    error = ""
    data = request.data
    dataDict = json.loads(data)
    stars = dataDict['review']
    offer_id = dataDict['offer_id']

    try:
        db.child(db_offers).child(offer_id).update({"review": stars, "status": "TERMINATED"})
    except Exception as ex:
        error = str(ex)

    my_response = {
        "response": {
            "result_id": offer_id,
            "result_message": "Review created",
            "error": error
        }
    }

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response


@app.route("/supplier/validate-offers", methods=['GET'])
def getAcceptedOffers():
    error = ""
    retrieved_offers = None
    validated_offers = []
    try:
        retrieved_offers = db.child(db_offers).get()
    except Exception as ex:
        error = str(ex)

    if error == "":
        validated_offers = validated_offers_eligible_for_feedback(retrieved_offers)

        for offer in validated_offers:
            if "review" not in offer:
                db.child(db_offers).child(offer["offer_id"]).update({"review": 0})

    my_response = {
        "response": {
            "results": validated_offers,
            "error": error
        }
    }

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response




# mock for adding an offer for a request
@app.route("/supplier/add-offer", methods=['POST'])
def addOfferForATransportRequest():
    data = request.data
    dataDict = json.loads(data)
    my_request_offer = dataDict['request_id']
    my_request_offer_price = random.randint(200, 1000)

    request_offer = {
        "offer_id": "",
        "request_id": my_request_offer,
        "price": my_request_offer_price,
        "estimate_arrival": time.mktime((datetime.now() + timedelta(hours=random.randint(3,18))).timetuple()),
        "timestamp": time.mktime(datetime.now().timetuple()),
        "status": "in_progress",
        "supplier_id": random.randint(0, 2)
    }

    error = ""
    try:
        response = db.child(db_offers).push(request_offer)
        request_offer["offer_id"] = response["name"]
        db.child(db_offers + "/" + response["name"]).update({"offer_id": response["name"]})
        pprint(response)
    except Exception as ex:
        error = str(ex)

    my_response = {
        "response": {
            "request_id": request_offer["request_id"],
            "offer_id": request_offer["offer_id"],
            "offer_payload": request_offer,
            "error": error
        }
    }

    if error == "":
        offers[request_offer["offer_id"]] = my_response

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response





# mock for starting a new transport event
@app.route("/supplier/add-transport", methods=['POST'])
def addTransportEvent():
    data = request.data
    dataDict = json.loads(data)
    offer = dataDict['offer_id']

    request_transport = {
        "offer_id": offer,
        "transport_id": "",
        "products": products,
        "timestamp": time.mktime(datetime.now().timetuple())
    }

    error = ""
    try:
        response = db.child(db_requests).push(request_transport)
        request_transport["transport_id"] = response["name"]
        pprint(response)
    except Exception as ex:
        error = str(ex)

    my_response = {
        "response": {
            "request_id": request_transport["request_id"],
            "request_payload": request_transport,
            "error": error
        }
    }

    if error == "":
        requests[request_transport["request_id"]] = my_response

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response

# mock for updating a transport event
@app.route("/supplier/update-transport", methods=['POST'])
def updateTransportEvent():
    pass

@app.route("/supplier/get-transports", methods=['GET'])
def getTransports():
    retrieved_transports = None
    error = ""
    try:
        retrieved_transports = db.child(db_transports).get()
    except Exception as ex:
        error = str(ex)

    if error == "":
        transports = retrieved_transports

    my_response = {
        "response": {
            "results": transports,
            "error": error
        }
    }

    response = app.response_class(
        response=json.dumps(my_response),
        mimetype='application/json'
    )
    return response

@app.route("/analytics/update-stocks", methods=['POST'])
def updateStocks():
    pass



def make_request_to_supplier(): pass

# Listener pe colectie
def receive_offer_from_supplier(): pass

# accept / decline it
def update_offer():pass

def get_transports(): pass

# Listen on transports
def get_child_update_transports(): pass

def analytics(): pass

def get_best_offers(): pass

@app.route('/search-assistance-service/stock', methods=['GET'])
def getStockRequest():
    stock_list = getStock()
    response = app.response_class(
        response= stock_list,
        mimetype='application/json'
    )
    return response

@app.route('/dev/upload-stock', methods=['POST'])
def uploadStockFromFile():
    file = request.files['file']
    # file_data = file.readlines()
    uploadStock(file)
    response = app.response_class(
        response= json.dumps("OK"),
        mimetype='application/json'
    )
    return response


def stream_handler(message):
   print(message)



db.child(db_offers).stream(stream_handler)


def mapper_firebase_application(firebase_data, key=None, value=None):
    got_data = []
    if firebase_data.firebases:
        for elem in firebase_data.firebases:
            element = elem.item[1]
            if key is not None and value is not None and element[key] != value:
                continue
            got_data.append(element)

    got_data.reverse()
    return got_data



def validated_offers_eligible_for_feedback(firebase_data):
    got_data = []
    # current_timestamp = time.mktime(datetime.now().timetuple())
    if firebase_data.firebases:
        for elem in firebase_data.firebases:
            element = elem.item[1]
            # delay = 0
            # estimate_arrival_timestamp = element["estimate_arrival"]
            # if "delay" in element:
            #     delay = element["delay"] * 3600
            if ("review" in element) or (element["status"] == "ACCEPTED") or (element["status"] == "TERMINATED"):
                got_data.append(element)

    got_data.reverse()
    return got_data


"""
ANALytics
"""


@app.route('/search-assistance-service/analytics/offers', methods=['GET'])
def get_offers():
    statistics = get_analytics_offers()
    response = app.response_class(
        response=statistics,
        mimetype='application/json'
    )
    return response


@app.route('/search-assistance-service/analytics/reponse-time', methods=['GET'])
def get_response_time():
    statistics = get_analytics_response_time()
    response = app.response_class(
        response=statistics,
        mimetype='application/json'
    )
    return response


@app.route('/search-assistance-service/analytics/eta', methods=['GET'])
def get_eta():
    statistics = get_analytics_eta()
    response = app.response_class(
        response=statistics,
        mimetype='application/json'
    )
    return response


@app.route('/search-assistance-service/analytics/delays', methods=['GET'])
def get_delays():
    statistics = get_analytics_delays()
    response = app.response_class(
        response=statistics,
        mimetype='application/json'
    )
    return response


@app.route('/search-assistance-service/analytics/delivery_time', methods=['GET'])
def get_delivery_time():
    statistics = get_analytics_delivery_time()
    response = app.response_class(
        response=statistics,
        mimetype='application/json'
    )
    return response


@app.route('/search-assistance-service/analytics/counts_evolution', methods=['GET'])
def get_evolution_counts():
    statistics = get_count_evolution()
    response = app.response_class(
        response=statistics,
        mimetype='application/json'
    )
    return response



def main():
    create_env()
    scheduled_thread = threading.Thread(target = start, args = ())
    scheduled_thread.start()
    app.run(host='0.0.0.0', port=app_port)


if __name__ == "__main__":
    main()




